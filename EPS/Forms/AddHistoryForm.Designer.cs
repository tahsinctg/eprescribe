﻿namespace EPS.Forms
{
    partial class AddHistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPatientHistory = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPatientHistory
            // 
            this.lblPatientHistory.AutoSize = true;
            this.lblPatientHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientHistory.Location = new System.Drawing.Point(86, 25);
            this.lblPatientHistory.Name = "lblPatientHistory";
            this.lblPatientHistory.Size = new System.Drawing.Size(109, 16);
            this.lblPatientHistory.TabIndex = 0;
            this.lblPatientHistory.Text = "Patient History";
            // 
            // AddHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.lblPatientHistory);
            this.Name = "AddHistoryForm";
            this.Text = "Add Patient History";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPatientHistory;
    }
}