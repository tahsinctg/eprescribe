/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : eprescribe

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2015-01-20 12:27:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for doctor
-- ----------------------------
DROP TABLE IF EXISTS `doctor`;
CREATE TABLE `doctor` (
  `DoctorID` varchar(255) NOT NULL,
  `UserID` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `Sex` bit(2) DEFAULT NULL,
  `Mobile` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Qualification` varchar(255) DEFAULT NULL,
  `Availavility` varchar(255) DEFAULT NULL,
  `UserType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DoctorID`),
  KEY `FKUserIDDoctor` (`UserID`),
  CONSTRAINT `FKUserIDDoctor` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of doctor
-- ----------------------------

-- ----------------------------
-- Table structure for patient
-- ----------------------------
DROP TABLE IF EXISTS `patient`;
CREATE TABLE `patient` (
  `PatientID` varchar(255) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Age` int(11) DEFAULT NULL,
  `Sex` bit(2) DEFAULT NULL,
  `Mobile` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `UserType` varchar(255) DEFAULT NULL,
  `UserID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`PatientID`),
  KEY `FKUserType` (`UserType`) USING BTREE,
  KEY `FKUserIDUser` (`UserID`),
  CONSTRAINT `FKUserIDUser` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`),
  CONSTRAINT `FKUserTypeUser` FOREIGN KEY (`UserType`) REFERENCES `user` (`UserType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of patient
-- ----------------------------

-- ----------------------------
-- Table structure for patienthistory
-- ----------------------------
DROP TABLE IF EXISTS `patienthistory`;
CREATE TABLE `patienthistory` (
  `HistoryID` varchar(255) NOT NULL,
  `UserID` varchar(255) NOT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Reports` blob,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`HistoryID`),
  KEY `FKUserID` (`UserID`),
  CONSTRAINT `FKUserID` FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of patienthistory
-- ----------------------------

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `PaymentID` int(11) NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `Amount` int(255) DEFAULT NULL,
  `DateofPayment` datetime DEFAULT NULL,
  `Status` bit(2) DEFAULT NULL,
  PRIMARY KEY (`PaymentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of payment
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `UserID` varchar(255) NOT NULL,
  `UserType` varchar(10) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  KEY `UserType` (`UserType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
